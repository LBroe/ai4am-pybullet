import argparse
from deep_qlearn.deep_q_learning import StableDeepQLearning
from stick_balancing_2d import utils



def main():
    parser = argparse.ArgumentParser(description="Use reinforcement learning to \
                                                  draw a circle.")
    parser.add_argument('-t', '--test', metavar='PATH',
                        type=str, help="Test an existing model.")
    parser.add_argument('-s', '--save', metavar='PATH', default="deep_circle.pkl",
                        type=str, help="Path to save the trained model.")
    parser.add_argument('-r', '--resume', action='store_true', default=False,
                        help="Continue training of an existing model.")
    parser.add_argument('-g', '--graphic', action='store_true', default=False,
                        help="Display graphical output.")

    args = parser.parse_args()
    dql = StableDeepQLearning()

    if args.test is not None:
        if utils.file_exists(args.test):
            dql.test(args.test)
        else:
            print("Model not found!")
    else:
        model_name = utils.get_empty_file_name(args.save, default="deep_circle.pkl")
        print(f"Model will be saved as {model_name}")
        dql.train(model_name, load=args.resume, renders=args.graphic)


if __name__ == '__main__':
    main()
