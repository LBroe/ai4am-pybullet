import pybullet as p
import time

TIMESTEP = 1/200


moving_objects = list()


def setup():
    physicsClient = p.connect(p.GUI)
    p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)  # GUI-Elemente ausschalten
    p.setGravity(0, 0, -9.81)
    planeId = p.loadURDF("plane.urdf")  # Boden
    p.setTimeStep(TIMESTEP)


def cy(x, y, z):
    obj_id = p.loadURDF("cylinder.urdf", [x, y, z], p.getQuaternionFromEuler([0, 0, 0]))
    moving_objects.append(obj_id)
    return obj_id


def sph(x, y, z):
    obj_id = p.loadURDF("sphere2.urdf", [x, y, z], p.getQuaternionFromEuler([0, 0, 0]))
    moving_objects.append(obj_id)
    return obj_id


def aufr():
    global moving_objects
    p.setGravity(-20, 5, 5)
    for i in range(round(2 / TIMESTEP)):
        t = time.time()
        p.stepSimulation()
        now = time.time()
        if TIMESTEP > (now - t):
            time.sleep(TIMESTEP - (now - t))
    for o in moving_objects:
        p.removeBody(o)
    moving_objects = list()
    p.setGravity(0, 0, -10)


def pyramide():
    cy(0, 0, 0.3)
    cy(0.4, 0, 0.3)
    cy(0.8, 0, 0.3)
    cy(-0.8, 0, 0.3)
    cy(-0.4, 0, 0.3)
    cy(0.2, 0, 0.9)
    cy(-0.2, 0, 0.9)
    cy(-0.6, 0, 0.9)
    cy(0.6, 0, 0.9)
    cy(0, 0, 1.5)
    cy(0.4, 0, 1.5)
    cy(-0.4, 0, 1.5)
    cy(-0.2, 0, 2.1)
    cy(0.2, 0, 2.1)
    cy(0, 0, 2.7)

    fliegi = sph(0, 6, 0.3)
    return fliegi


def schubs(geschubst, force):
    pos, orient = p.getBasePositionAndOrientation(geschubst)
    p.applyExternalForce(geschubst, -1, force, pos, p.WORLD_FRAME)

def werf(slomo):
    fliegi = pyramide()  # Gibt ID von Ball zurück
    modifier = 1
    for i in range(round(6 / TIMESTEP)):
        if 3 <= i * TIMESTEP <= 3.1:
            schubs(fliegi, [0, -3000, 600])
            print("pushing")

        if 3.13 <= i * TIMESTEP <= 3.7 and slomo:  # Zeitlupe zwischen 3.13 und 3.7 sekunden
            modifier = 20
        else:
            modifier = 1
        t = time.time()
        p.stepSimulation()
        now = time.time()
        if TIMESTEP > (now - t):  # Warten, bis Realität Simulation eingeholt hat
            time.sleep((TIMESTEP - (now - t)) * modifier)

def main():
    setup()
    p.resetDebugVisualizerCamera(9, 180, -22, (0.5199999809265137, 0, 0))
    werf(False)
    aufr()
    p.resetDebugVisualizerCamera(9, 100, -13, (0.5199999809265137, 0, 0))
    werf(True)


if __name__ == "__main__":
    main()
