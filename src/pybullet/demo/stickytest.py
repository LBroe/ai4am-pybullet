import math
import pybullet as p
import time

TIMESTEP = 1 / 200


def setup():
    physicsClient = p.connect(p.GUI)  # or p.DIRECT for non-graphical version
    p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
    p.setGravity(0, 0, -10)
    planeId = p.loadURDF("plane.urdf")
    cubeStartPos = [0, 0, 1]
    p.setTimeStep(TIMESTEP)


def print_filament(x, y, z):
    obj_id = p.loadURDF("filament.urdf", [x, y, z], p.getQuaternionFromEuler([0, 0, 0]), useFixedBase=False)
    return obj_id


def link(s1, s2=None):
    pos1, or1 = p.getBasePositionAndOrientation(s1)
    if s2 is None:
        or2 = p.getQuaternionFromEuler([0, 0, 0])
        dist = pos1
    else:
        pos2, or2 = p.getBasePositionAndOrientation(s2)
        dist = [(x[0] - x[1]) for x in zip(pos1, pos2)]
    return p.createConstraint(s1, -1, s2, -1, p.JOINT_FIXED, [0, 0, 0], [0, 0, 0], dist, or2, or1)


def circle_points(c, r, n):
    return [(c[0] + math.cos(2 * math.pi / n * x) * r, c[1] + math.sin(2 * math.pi / n * x) * r, c[2]) for x in
            range(0, n + 1)]


def main():
    setup()

    extrude = True
    extruder_position = [0, 0, 1]
    circle = circle_points([0, 0, 0.1], 0.2, 100)

    sticky_spheres = list()
    constraints = dict()

    for i in range(50000000):
        if extrude:
            extruder_position = circle[i % len(circle)]
            sticky_spheres.insert(0, print_filament(extruder_position[0], extruder_position[1], 0.01 + i / 5000))
            if len(sticky_spheres) > 50:
                sticky_spheres.pop()
        for s in sticky_spheres:
            for con in p.getContactPoints(s):
                if frozenset([s, con[2]]) not in constraints:
                    constraints[frozenset([s, con[2]])] = link(s, con[2])
        t = time.time()
        p.stepSimulation()
        now = time.time()
        if TIMESTEP > (now - t):
            time.sleep(TIMESTEP - (now - t))


if __name__ == "__main__":
    main()
