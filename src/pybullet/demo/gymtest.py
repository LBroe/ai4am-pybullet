import gym
import time

import pybullet_envs.bullet.racecarGymEnv as e

env = e.RacecarGymEnv(isDiscrete=False, renders=True)
e.pybullet.configureDebugVisualizer(e.pybullet.COV_ENABLE_GUI, 0)
env.reset()

direction = 1
startTime = time.time()

if __name__ == "__main__":
    while True:
        action = env.action_space.sample()  # random action
        action[0] = 0.5 * direction
        if time.time() - startTime > 3:
            startTime = time.time()
            direction = 0 - direction
        observation, reward, done, info = env.step(action)
