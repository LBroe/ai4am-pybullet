import threading
from vangogh import observer


# subclass of Thread to run the tracking in an own thread
# has an observer as attribute and does the tracking as its thread starts
# (run method is overwritten with the tracking function)


class Holmes(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.observer = observer.Observer()

    def run(self):
        self.observer.tracking()

    def get_old_position(self):
        return self.observer.old_position

    def get_actual_position(self):
        return self.observer.actual_position

    # reset the position to the point when the printer is centered (look up printer.middle() function!)
    def reset(self):
        self.observer.old_position = [435, 242]

    # computes the feedback from the tracking for the q-learning
    def transform_action(self):
        diffx = (self.observer.actual_position[0] - self.observer.old_position[0]) / 3
        diffy = (self.observer.actual_position[1] - self.observer.old_position[1]) / 3
        self.observer.old_position[0] = self.observer.actual_position[0].copy()
        self.observer.old_position[1] = self.observer.actual_position[1].copy()
        return [diffy, diffx]

    def stop(self):
        self.observer.running = False
