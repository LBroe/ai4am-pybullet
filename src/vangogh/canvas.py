import numpy as np
import matplotlib
from matplotlib import pyplot as plt

class Easel:
    """
    A place to draw on. Has a painting area (canvas) and a position (brush).
    """

    def __init__(self, w, h, brushwidth=3):
        self.w = w
        self.h = h
        self.canvas = np.zeros([w, h])
        self.brush = [w//2, h//2]
        self.brushwidth = brushwidth

    def draw(self, dx, dy):
        """
        Draws on the canvas with relative coordinates.
        """

        length = np.linalg.norm([dx, dy])

        for _ in range(int(length)):
            self.move(dx/length, dy/length)
            self.paint()
        self.move(dx - (dx/length) * int(length), dy - (dy / length) * int(length))
        self.paint()

    def drawTo(self, x, y):
        """
        Draws on the canvas with absolute coordinates.
        """
        self.draw(x - self.brush[0], y - self.brush[1])

    def move(self, dx, dy):
        """
        Moves the brush with relative coordinates
        """
        self.brush[0] += dx
        self.brush[1] += dy
        self.brush[0] = max(0, min(self.w - 1, self.brush[0]))
        self.brush[1] = max(0, min(self.h - 1, self.brush[1]))

    def moveTo(self, x, y):
        """
        Moves the brush with absolute coordinates
        """
        self.move(x - self.brush[0], y - self.brush[1])

    def paint(self):
        """
        Paints a square on the current cursor position
        """
        upper = int(round(max(0, self.brush[0] - self.brushwidth / 2)))
        left  = int(round(max(0, self.brush[1] - self.brushwidth / 2)))
        lower = int(round(min(self.h - 1, self.brush[0] + self.brushwidth / 2))) + 1
        right = int(round(min(self.w - 1, self.brush[1] + self.brushwidth / 2))) + 1
        self.canvas[upper:lower, left:right] = 1

    def show(self):
        """
        Shows the canvas
        """
        plt.imshow(self.canvas)
        plt.pause(0.00001)
