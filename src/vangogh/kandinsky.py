from vangogh import canvas
from vangogh import hough
import numpy as np
from matplotlib import pyplot as plt
import random
import math

class EaselAtelier:
    """
    An atelier for any agent that draws on an easel.
    Can integrate a 3D printer (see printer.py) and sensors (see holmes.py).
    """

    def __init__(self, canvas_size=(200, 200), brushwidth=3, printer=None, holmes=None):
        self.easel = canvas.Easel(*canvas_size, brushwidth)
        self.printer = printer
        self.holmes = holmes
        if printer:

            printer.middle()

    def step(self, action, print=True):
        if self.printer:
            # the 1.1 is a factor to reduce the action the printer does, so the circle gets a little bit smaller
            self.printer.move([action[1]/1.1, -action[0]/1.1, 0], False)

        if self.holmes:
            action = self.holmes.transform_action()

        if print:
            self.easel.draw(*action)
        else:
            self.easel.move(*action)

        return 0


class KandinskyAtelier(EaselAtelier):
    """
    Environment for a q learning agent learning to draw circles.
    The agent is supposed to draw a circle of given radius and abritrary position.
    The observation and reward are based on the 'best circle', which is the Hough maximum of the already drawn pattern.

    Deprecated in favor of KandinskyAtelier2
    """
    def __init__(self, radius, movement_cost, **kwargs):
        super().__init__(**kwargs)
        self.old_hough_max = 0
        self.radius = radius
        self.movement_cost = movement_cost
        self.hough = hough.Hough(self.radius)
        self.poi = None
        self.theta = 0


    def step(self, action, print=True):
        action = (math.cos(self.theta) * action[0] - math.sin(self.theta) * action[1], math.sin(self.theta) * action[0] + math.cos(self.theta) * action[1])
        old_brush = np.copy(self.easel.brush)

        super().step(action, print)

        blurred = self.hough.transform(self.easel.canvas)

        new_hough_max = np.max(blurred)
        self.poi = np.unravel_index(np.argmax(blurred), blurred.shape)

        reward = new_hough_max - self.old_hough_max - self.movement_cost * np.linalg.norm(action)

        self.old_hough_max = new_hough_max

        movement = np.subtract(self.easel.brush, old_brush)
        self.theta = np.arctan2(movement[1], movement[0])
        return reward


    def observe(self):
        if self.poi is None:
            return (0, 0)

        o = np.subtract(self.poi, self.easel.brush)
        o = (math.cos(-self.theta) * o[0] - math.sin(-self.theta) * o[1],
             math.sin(-self.theta) * o[0] + math.cos(-self.theta) * o[1])
        return o


    def show(self):
        plt.imshow(self.hough.transform(self.easel.canvas))
        plt.figure()
        plt.imshow(self.easel.canvas)
        plt.show()

class KandinskyAtelier2(EaselAtelier):
    """
    Environment for a q learning agent learning to draw circles with rotational invariance.
    The agent is supposed to draw a circle of given radius and circle center.
    In this environment, the agent has a viewing direction and can steer. The observation is the relative
    position of the selected center rotated by the viewing direction.

    Named after Wassily Kandinsky because of his painting 'Squares With Concentric Circles'
    """

    def __init__(self, radius_bounds, poi=None, deepq=False, **kwargs):
        super().__init__(**kwargs)
        self.min_radius = radius_bounds[0]
        self.max_radius = radius_bounds[1]
        self.poi = poi
        self.deepq = deepq
        self.old_score = 0
        self.theta = 0
        if self.poi is None:
            self.poi = (random.random() * self.easel.w/10 + 0.45 * self.easel.w, random.random() * self.easel.h/10 + 0.45 * self.easel.h)

    def step(self, action, print=True):
        action = (math.cos(self.theta) * action[0] - math.sin(self.theta) * action[1], math.sin(self.theta) * action[0] + math.cos(self.theta) * action[1])

        old_brush = np.copy(self.easel.brush)
        super().step(action, print)
        score = 0
        for x, y in np.argwhere(self.easel.canvas > 0):
            d = (self.poi[0] - x)**2 + (self.poi[1] - y)**2
            if self.min_radius**2 <= d <= self.max_radius**2:
                score += 1  # Reward for Drawing on the circle
            else:  # Punishment for not drawing on the circle
                if self.deepq:
                    score -= 0.01
                else:
                    score -= 0

        reward = score - self.old_score
        distance = math.sqrt(np.linalg.norm(np.subtract(self.poi, self.easel.brush)))
        reward -= abs(distance - (self.max_radius + self.min_radius) / 2) * 0.01
        self.old_score = score

        movement = np.subtract(self.easel.brush, old_brush)
        self.theta = np.arctan2(movement[1], movement[0])

        return reward

    def observe(self):
        if self.poi is None:
            return (0, 0)

        o = np.subtract(self.poi, self.easel.brush)
        o = (math.cos(-self.theta) * o[0] - math.sin(-self.theta) * o[1], math.sin(-self.theta) * o[0] + math.cos(-self.theta) * o[1])
        return o

    def show(self):
        c = np.copy(self.easel.canvas)
        p0 = int(self.poi[0])
        p1 = int(self.poi[1])
        c[p0 - 1 : p0 + 1, p1 - 1 : p1 + 1] = -1
        plt.imshow(c)
        plt.pause(0.0001)
