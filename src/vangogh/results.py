from matplotlib import pyplot as plt
from scipy.ndimage.filters import gaussian_filter, median_filter
import numpy as np

def plot_results(results, save=False):
    """
    Plots training results collected by main.py
    """
    fig, ax1 = plt.subplots(figsize=(10, 7))

    ax1.set_xlabel("Iteration")
    #ax1.set_xlim(0, 9000)
    ax1.set_ylabel("Score", color="blue")
    #ax1.set_ylim(0, 260)
    no_epsilon_scores = list(map(lambda x: float(x[2]), results))
    median_scores = gaussian_filter(median_filter(no_epsilon_scores, size=1000, mode="nearest"), sigma=1)
    ax1.scatter(list(range(len(results))), no_epsilon_scores, label="Score without epsilon", color="silver", s=5)
    ax1.plot(median_scores, label="Score without epsilon, filtered", color="blue", linewidth=2)
    #ax1.plot(median_filter(list(map(lambda x: x[1], results)), size=1000), "--", label="Score with epsilon", color="blue")
    ax1.tick_params(axis="y", labelcolor="blue")

    # Standard deviation
    #dev = [np.std(no_epsilon_scores[max(0, i-500):min(len(results), i + 500)]) for i in range(len(no_epsilon_scores))]
    #ax1.plot([median_scores[i] + dev[i] for i in range(len(results))], label="Median + Standard deviation", color="steelblue")
    #ax1.plot([median_scores[i] - dev[i] for i in range(len(results))], label="Median - Standard deviation", color="steelblue")

    ax2 = ax1.twinx()
    ax2.set_ylim(0, 1)

    ax2.set_ylabel("Epsilon in %", color="red")
    ax2.plot(list(map(lambda x: x[0], results)), label="Epsilon", color="red")
    #ax2.plot(list(map(lambda x: x[3], results)), "--", label="Non-Null", color=color)
    ax2.tick_params(axis="y", labelcolor="red")
    #ax2.legend()

    #plt.title("Training")

    fig.tight_layout()
    if save:
        plt.savefig("results.pdf", format="pdf")
    else:
        plt.show()

if __name__ == "__main__":
    with open("agents/results_bond.txt") as dat:
        plot_results(eval(dat.read()), save=False)