
import numpy as np
from math import sin, cos

# Look at section 4.2.1 of the final document for an explanation of these values
ACTION_DIRECTION_COUNT = 64
OBSERVE_DIRECTION_COUNT = 64
OBSERVE_LENGTH_COUNT = 50
OBSERVE_LENGTH_STEP = 1

class MondrianAtelier:
    """
    A 'proxy environment' for q learning, which takes a real environment and discretizes the actions
    and the observations.

    Named after Piet Mondrian because he paints rectangles that look very discrete.
    """

    def __init__(self, atelier):
        self.atelier = atelier
        self.action_direction_count = ACTION_DIRECTION_COUNT
        self.observe_direction_count = OBSERVE_DIRECTION_COUNT
        self.observe_length_count = OBSERVE_LENGTH_COUNT
        self.observe_length_step = OBSERVE_LENGTH_STEP


    def step(self, i):
        x = cos(2 * np.pi / self.action_direction_count * i)
        y = sin(2 * np.pi / self.action_direction_count * i)

        return self.atelier.step((5 * x, 5 * y))


    def observe(self):
        o = self.atelier.observe()
        length = np.linalg.norm(o)
        theta = (np.arctan2(o[1], o[0]) + np.pi)

        length_i = np.min([int(length / self.observe_length_step), self.observe_length_count - 1])
        theta_i = int(theta / (2 * np.pi / self.observe_direction_count))

        return length_i + theta_i * self.observe_length_count

    def action_count(self):
        return self.action_direction_count


    def observation_count(self):
        return self.observe_length_count * self.observe_direction_count
