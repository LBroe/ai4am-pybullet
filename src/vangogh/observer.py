import numpy as np
import cv2
import pyrealsense2 as rs


# observer class which does the tracking of the 3d printer


class Observer:

    def __init__(self):
        self.running = True
        self.old_position = [435, 242]
        self.actual_position = [0, 0]

    def tracking(self):
        counter = 0
        trackers = cv2.MultiTracker_create()

        # configuration of the realsense-camera
        pipeline = rs.pipeline()
        config = rs.config()
        config.enable_stream(rs.stream.color, 960, 540,
                             rs.format.bgr8, 60)  # args: stream type, width, height, format, fps
        pipeline.start(config)

        # streaming loop
        while self.running:
            frames = pipeline.wait_for_frames()
            image = frames.get_color_frame()
            frame = np.asanyarray(image.get_data())
            frame = cv2.flip(frame, 1)  # flip the frame so it matches the view if you stand in front of the 3d printer

            # wait until first images arrive, therefore wait 10 iterations
            if counter == 10:
                box1 = (385, 417, 86, 57)   # bounding box of the printhead so it matches the QR-Code
                box2 = (575, 240, 69, 42)   # bounding box of the printbed so it matches the QR-Code

                bboxes = [box1, box2]
                for bbox in bboxes:
                    # create a new object tracker for the bounding box and add it
                    # to our multi-object tracker
                    tracker = cv2.TrackerKCF_create()
                    trackers.add(tracker, frame, bbox)

            (success, boxes) = trackers.update(frame)

            # bounding box of the print head
            if len(boxes) > 0 and success:
                b0 = boxes[0]
                (x, y, w, h) = [int(v) for v in b0]
                center0 = (int(x + w / 2), int(y + h / 2))
                self.actual_position[0] = center0[0]    # X Position

            # bounding box of the marker on the printbed
            if len(boxes) > 1 and success:
                b1 = boxes[1]
                (x, y, w, h) = [int(v) for v in b1]
                center1 = (int(x + w / 2), int(y + h / 2))
                self.actual_position[1] = center1[1]    # Y Position

            if counter < 11:
                counter += 1

        pipeline.stop()
