from vangogh.printrun.printcore import printcore
from vangogh.printrun.plugins.sample import SampleHandler
import numpy as np
import time

number_of_oks = 0
position_recieved = False

class OurHandler(SampleHandler):
    """
    Handler for messages from the 3D printer.
    Notices, when a position is sent, which is used for synchronizing with the printer.
    """
    def on_recv(self, line):
        global number_of_oks, position_recieved

        if line.startswith("X"):
            print("Position recieved")
            position_recieved = True

class Printer:
    """
    Connection to a 3D printer. Sends G-Code commands over USB.
    """
    def __init__(self, heat = True):
        self.p = printcore('/dev/ttyUSB0', 115200)
        self.p.event_handler.append(OurHandler())
        time.sleep(1)
        self.set_movement_relative(True)
        if heat:
            self.heat()
        self.send("M117 Jetzt geht's rund!")

    def heat(self, bed_temperature=60, extruder_temperature=200):
        """
        Heats up the bed and the extruder simultaniously and waits for them.
        Homes the printer.
        """
        self.fan()
        self.send("M104 S{} T0".format(extruder_temperature))
        self.send("M140 S{}".format(bed_temperature))
        self.home()
        self.send("M190 S{}".format(bed_temperature)) # wait for bed
        self.send("M109 S{} T0".format(extruder_temperature)) # wait for extruder
        self.wait_for_completion()


    def fan(self, speed=255):
        self.send("M106 S{}".format(speed))

    def set_movement_relative(self, relative):
        if relative:
            self.send("G91")
        else:
            self.send("G90")

    def middle(self):
        self.set_movement_relative(False)
        self.move([120, 120, 0.5])
        self.set_movement_relative(True)
        self.wait_for_completion()


    def home(self):
        self.send("G28")
        time.sleep(2)
        self.wait_for_completion()

    def move(self, vec, filament=False, speed=3600):
        filament_per_unit = 0.30
        sleep_per_unit = 0.03
        move_length = np.linalg.norm(vec)

        if filament:
            extrude_filament = move_length * filament_per_unit
            self.send("G1 X{} Y{} Z{} E{} F{}".format(round(vec[0], 3), round(vec[1], 3), round(vec[2], 3), round(extrude_filament, 3), round(speed, 3)))
        else:
            self.send("G0 X{} Y{} Z{} F{}".format(round(vec[0], 3), round(vec[1], 3), round(vec[2], 3), round(speed, 3)))
        #time.sleep(sleep_per_unit * move_length)
        self.wait_for_completion()


    def wait_for_completion(self):
        """
        Waits until all previous commands are completed
        """
        global position_recieved
        position_recieved = False
        self.send("M400")
        self.send("M114")
        while not position_recieved:
            time.sleep(0.01)


    def send(self, command):
        """
        Sends a G-Code command
        """
        print("Sending {}".format(command))
        self.p.send(command)
