
import numpy as np
import random

class Artist:
    """
    A q learning agent.
    """

    def __init__(self, observation_count, action_count):
        self.table = np.zeros((observation_count, action_count))
        self.alpha = 0.2
        self.gamma = 0.99

    def learn(self, observation, action, new_observation, reward):
        self.table[observation, action] = (1 - self.alpha) * self.table[observation, action] + self.alpha * (reward + self.gamma * np.max(self.table[new_observation]))

    def best_action(self, observation):
        return np.argmax(self.table[observation])

    def save(self, path):
        np.savetxt(path, self.table)

    def load(self, path):
        self.table = np.loadtxt(path)


class EpsilonGreedyLearning:
    """
    Epsilon-greedily trains an agent on an environment.
    """

    def __init__(self, atelier, artist, epsilon):
        self.atelier = atelier
        self.artist = artist
        self.epsilon = epsilon


    def choose_action(self):
        r = random.random()
        if r >= self.epsilon:
            action = self.artist.best_action(self.atelier.observe())
        else:
            action = int(self.atelier.action_count() * random.random())
        return action


    def learn(self, change_agent=True):
        observation = self.atelier.observe()
        action = self.choose_action()
        reward = self.atelier.step(action)
        new_observation = self.atelier.observe()
        if change_agent:
            self.artist.learn(observation, action, new_observation, reward)
