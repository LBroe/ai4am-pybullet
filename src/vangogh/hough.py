import numpy as np
from scipy import ndimage
import math

DELTA_THETA = math.pi / 100

class Hough:
    """
    Transform a canvas to its Hough space representation for a given circle radius.
    Caches the result and calculates results of additive changes efficiently.
    The blur intensity varies with the radius.
    """

    def __init__(self, radius):
        self.radius = radius
        self.unblurred = None
        self.blurred = None
        self.last_canvas = None
        self.sigma = radius / 5

    def transform(self, canvas):
        """
        Transforms a canvas into Hough space.
        If no pixels were substracted, the last transformation is used for fast results.
        """
        if self.unblurred is None or self.last_canvas is None:
            return self._transform_new(canvas)

        diff_canvas = canvas - self.last_canvas

        if np.min(diff_canvas) < 0:
            return self._transform_new(canvas)

        if np.max(diff_canvas) == 0:
            return self.blurred

        for x in range(diff_canvas.shape[0]):
            for y in range(diff_canvas.shape[1]):
                if diff_canvas[x, y] > 0:
                    new_circle = np.zeros(diff_canvas.shape)
                    for theta in np.arange(0, 2*np.pi, DELTA_THETA):
                        a = x - self.radius * math.cos(theta)
                        b = y - self.radius * math.sin(theta)
                        if 0 <= round(a) < new_circle.shape[0] and 0 <= round(b) < new_circle.shape[1]:
                            new_circle[round(a), round(b)] = 1
                    self.unblurred += new_circle

        self.last_canvas = np.copy(canvas)

        self.blurred = ndimage.gaussian_filter(self.unblurred, sigma=self.sigma)
        return self.blurred


    def _transform_new(self, canvas):
        self.last_canvas = np.copy(canvas)
        self.unblurred = np.zeros(canvas.shape)

        for x in range(canvas.shape[0]):
            for y in range(canvas.shape[1]):
                if canvas[x, y] > 0:
                    new_circle = np.zeros(canvas.shape)
                    for theta in np.arange(0, 2*np.pi, DELTA_THETA):
                        a = x - self.radius * math.cos(theta)
                        b = y - self.radius * math.sin(theta)
                        new_circle[round(a), round(b)] = 1
                    self.unblurred += new_circle

        H = ndimage.gaussian_filter(self.unblurred, sigma=self.sigma)
        self.blurred = H
        return H


