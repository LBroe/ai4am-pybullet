from stick_balancing_2d.cartpole_2d import CartPole2DBulletEnv
from stable_baselines import DQN
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.deepq.policies import MlpPolicy
import time
from shutil import move


class DeepQLearning:
    """
    This class encapsulates the training and testing agents.
    """
    def __init__(self):
        pass

    def train(self, model_name: str = '2d_stick_balancing.model',
              renders: bool = True):
        """Trains an agent with DeepQ-Learning. Requires a path for the model
        to be saved to.
        """
        # create the environment
        env = CartPole2DBulletEnv(renders=renders)
        env = DummyVecEnv([lambda :env])

        model = DQN(MlpPolicy,env,
                    verbose=2,
                    learning_rate=5e-5,
                    exploration_fraction=0.5,
                    exploration_final_eps=0.001,
                    buffer_size=10000,
                    checkpoint_path='.',
                    checkpoint_freq=10
                    )
        model = model.learn(total_timesteps=150000)
        model.save(model_name)
        print("Model saved")

    def test(self, model_name: str = '2d_stick_balancing.model'):
        """Loads a model and simulates its behavior. Requires a path to a model"""
        # create the environment
        env = CartPole2DBulletEnv(renders=True)
        env = DummyVecEnv([lambda :env])

        model = DQN.load(model_name)

        obs = env.reset()
        while True:
            action, _states = model.predict(obs)
            obs, reward, done, info = env.step(action)
            time.sleep(0.01)


if __name__ == '__main__':
    DeepQLearning().train(renders=False)
    # DeepQLearning().test()
