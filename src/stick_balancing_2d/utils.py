from pathlib import Path


def get_empty_file_name(name: str, default: str = 'file.pkl') -> str:
    """
    Returns an unused filename.
    """
    if name is None or name == '':
        name = default
    p = Path(name)
    i = 1
    while p.exists():
        p = Path(p.parent, f'{Path(name).stem}_{i}{p.suffix}')
        i += 1
    return p.as_posix()


def file_exists(path: str) -> bool:
    """
    Checks if a file exists.
    """
    return Path(path).is_file()
