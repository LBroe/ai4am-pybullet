import math
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
import time
import pybullet as p


class CartPole2DBulletEnv(gym.Env):
    """
    Augmented cart-pole system for 2 dimensions. This class uses the cartpole_2d.urdf
    as a basic model.
    """
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 50
    }

    def __init__(self,
                 renders: bool = True,
                 actions: list = [-10, 0, 10], # 3 possible actions; left, no force, right
                 max_steps: int = 5000,
                 urdf_file: str = "stick_balancing_2d/cartpole_2d.urdf",
                 threshold: float = 2.0,
                 threshold_radians: int = 30):
        self.max_steps = max_steps
        self.urdf_file = urdf_file

        # start the bullet physics server
        self._renders = renders
        if renders:
            p.connect(p.GUI)
        else:
            p.connect(p.DIRECT)

        # Action space
        self.forces = []
        for x in actions:
            for y in actions:
                self.forces.append((x, y))
        # create Action Space. This can be seen as a 3x3 grid of possible movements,
        # 3 for x- and y-dimension each
        self.action_space = spaces.Discrete(len(self.forces))

        # Observation space; create thresholds for each observable quantity
        self._observation = []
        self.x_theta_threshold_radians = threshold_radians * 2 * math.pi / 360
        self.y_theta_threshold_radians = self.x_theta_threshold_radians
        self.x_threshold = threshold
        self.y_threshold = self.x_threshold
        bound_high = np.array([
            self.x_threshold,                   # x position
            np.finfo(np.float32).max,           # x velocity
            self.y_threshold,                   # y position
            np.finfo(np.float32).max,           # y velocity
            self.x_theta_threshold_radians,
            np.finfo(np.float32).max,
            self.y_theta_threshold_radians,
            np.finfo(np.float32).max,
        ])
        self.observation_space = spaces.Box(-bound_high, bound_high, dtype=np.float32)

        self.seed()
        # load model
        self.cartpole = p.loadURDF(self.urdf_file)
        self.reset()

        self.viewer = None
        self._configure()
    # methods required by gym.Env
    def _configure(self, display=None):
        self.display = display

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]


    def step(self, action):
        """
        Simulate one step. Requires a action from the action. Also calculates the reward
        and returns this together with the next state.
        """
        # Move cart
        x_force, y_force = self.forces[action]
        p.setJointMotorControl2(bodyUniqueId=self.cartpole,
                                jointIndex=0,
                                controlMode=p.TORQUE_CONTROL,
                                force=x_force)
        p.setJointMotorControl2(bodyUniqueId=self.cartpole,
                                jointIndex=1,
                                controlMode=p.TORQUE_CONTROL,
                                force=y_force)

        # Calculate new state
        p.stepSimulation()
        self.state = p.getJointState(self.cartpole, jointIndex=0)[0:2] \
                     + p.getJointState(self.cartpole, jointIndex=1)[0:2] \
                     + p.getJointState(self.cartpole, jointIndex=2)[0:2] \
                     + p.getJointState(self.cartpole, jointIndex=3)[0:2]

        # Calculate reward
        reward = 1.0 - abs(self.state[4])*3 - abs(self.state[6])*3 \
                 - abs(self.state[0])*0.1 - abs(self.state[2])*0.1

        self.steps += 1

        # Check episode end
        done = (np.array(self.state) > self.observation_space.high).any() or \
               (np.array(self.state) < self.observation_space.low).any() or \
               self.steps >= self.max_steps

        # Observation, reward, done, info
        return np.array(self.state), reward, done, {}

    def reset(self):
        """
        Resets the simulation. This is called after each episode has ended.
        """
        self.steps = 0

        # Reset damping for all links
        damping = 0
        p.changeDynamics(self.cartpole, -1, linearDamping=damping, angularDamping=damping)
        p.changeDynamics(self.cartpole, 0, linearDamping=damping, angularDamping=damping)
        p.changeDynamics(self.cartpole, 1, linearDamping=damping, angularDamping=damping)
        p.changeDynamics(self.cartpole, 2, linearDamping=damping, angularDamping=damping)
        p.changeDynamics(self.cartpole, 3, linearDamping=damping, angularDamping=damping)

        # Reset forces of all joints
        p.setJointMotorControl2(self.cartpole, 0, p.VELOCITY_CONTROL, force=0)
        p.setJointMotorControl2(self.cartpole, 1, p.VELOCITY_CONTROL, force=0)
        p.setJointMotorControl2(self.cartpole, 2, p.VELOCITY_CONTROL, force=0)
        p.setJointMotorControl2(self.cartpole, 3, p.VELOCITY_CONTROL, force=0)

        # Environment settings
        p.setGravity(0, 0, -9.8)
        self.timeStep = 0.02
        p.setTimeStep(self.timeStep)
        p.setRealTimeSimulation(0)

        # Slightly randomized starting position
        randstate = self.np_random.uniform(low=-0.05, high=0.05, size=(8,))
        p.resetJointState(self.cartpole, 0, randstate[0]*5, randstate[1])
        p.resetJointState(self.cartpole, 1, randstate[2]*5, randstate[3])
        p.resetJointState(self.cartpole, 2, randstate[4]*5, randstate[5])
        p.resetJointState(self.cartpole, 3, randstate[6]*5, randstate[7])

        # New state
        self.state = p.getJointState(self.cartpole, jointIndex=0)[0:2] \
                     + p.getJointState(self.cartpole, jointIndex=1)[0:2] \
                     + p.getJointState(self.cartpole, jointIndex=2)[0:2] \
                     + p.getJointState(self.cartpole, jointIndex=3)[0:2]
        return np.array(self.state)

    def render(self, mode='human', close=False):
        pass


if __name__ == '__main__':
    env = CartPole2DBulletEnv()
    env.render()
    while True:
        _, _, done, _ = env.step(4)
        time.sleep(0.02)
        if done:
            env.reset()
