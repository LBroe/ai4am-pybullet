import numpy as np
from vangogh import kandinsky
from vangogh import mondrian
from vangogh import qlearning
from vangogh.results import plot_results
import os
import time

"""
Loads an agent from a text file (bond.txt) and trains it, saves the result in the text file again.
"""

# Directory for agents and results
directory = "agents"

if not os.path.exists(directory):
    os.makedirs(directory)

filename = "bond.txt"

TRAIN = True  # If True, trains the agent. If False, shows the circles the agent makes

# Training Hyperparameters
START = 0  # Where to start the iterations. When resuming training, set to last iteration before interrupt
ITERATIONS = 10000  # Total number of training iterations
DESCENT = 0.8  # Fraction of the iterations where epsilon decreases. The other iterations are done with minimum epsilon
MINIMUM = 0.05  # The lowest epsilon, used in the last ITERATIONS * (1 - DESCENT) iterations

STEPS_PER_ITERATION = 50  # How many steps the agent has to try to draw a circle

print("Training Values: ", START, ITERATIONS, DESCENT, MINIMUM, STEPS_PER_ITERATION)

PRINT = False  # If the agent should print on a physical 3D printer

if PRINT:
    import holmes
    import printer

    p = printer.Printer(heat=False)
    p.middle()
    time.sleep(5)
    h = holmes.Holmes()
    h.start()
else:
    p = None
    h = None

print("Atelier Values: ", mondrian.ACTION_DIRECTION_COUNT, mondrian.OBSERVE_DIRECTION_COUNT,
      mondrian.OBSERVE_LENGTH_COUNT, mondrian.OBSERVE_LENGTH_STEP)
artist = qlearning.Artist(mondrian.OBSERVE_LENGTH_COUNT * mondrian.OBSERVE_DIRECTION_COUNT,
                          mondrian.ACTION_DIRECTION_COUNT)
if os.path.isfile(os.path.join(directory, filename)):
    artist.load(os.path.join(directory, filename))

if __name__ == "__main__":
    egl = None
    try:
        if TRAIN:
            starting = time.time()
            results = list()  # List of the epsilon, the score, the score without epsilon and the fraction of cells in the agent's table which are not 0 for every iteration
        for i in range(START, ITERATIONS):
            print("\n\nIteration: {}/{} ({:.1f}%)".format(i, ITERATIONS, 100 * i / ITERATIONS))

            if i > START and TRAIN:
                seconds_left = ((time.time() - starting) / (i - START)) * (ITERATIONS - i)
                hours = int(seconds_left // 60 ** 2)
                minutes = int((seconds_left // 60) % 60)
                seconds = int(seconds_left % 60)
                print("Estimated time left: {:2d}h {:2d}m {:2d}s".format(hours, minutes, seconds))

            # epsilon decreases linearly
            epsilon = max(MINIMUM, (ITERATIONS * DESCENT - i) / (ITERATIONS * DESCENT)) if TRAIN else 0
            print("Epsilon: {:.3f}".format(epsilon))
            atelier = mondrian.MondrianAtelier(kandinsky.KandinskyAtelier2((41, 42), canvas_size=(200, 200), printer=p, holmes=h, brushwidth=1))
            egl = qlearning.EpsilonGreedyLearning(atelier, artist, epsilon)

            for j in range(STEPS_PER_ITERATION):
                egl.learn()

            if TRAIN:  # This is fast and interesting, so why not?
                no_epsilon_atelier = mondrian.MondrianAtelier(
                    kandinsky.KandinskyAtelier2((41, 42), canvas_size=(200, 200), brushwidth=1))
                no_epsilon_egl = qlearning.EpsilonGreedyLearning(no_epsilon_atelier, artist, 0)
                for j in range(STEPS_PER_ITERATION):
                    no_epsilon_egl.learn(False)
                no_epsilon_score = no_epsilon_atelier.atelier.old_score
                print("Score without epsilon:", no_epsilon_score)

            if (i + 1) % 300 == 0 and TRAIN:  # Saves the Agent and the results every 300th iteration
                artist.save(os.path.join(directory, filename))
                with open(os.path.join(directory, "results_{}.txt".format(filename.replace(".txt", ""))), "w") as dat:
                    dat.write("# Training Values: Start: {}, Iterations: {}, Descent: {}, Minimum: {}\n".format(START, ITERATIONS, DESCENT, MINIMUM))
                    dat.write("# Atelier Values: Action Direction: {}, Observation Direction: {}, Observation Lenght: {}, Observation Length Count: {}\n".format(
                            atelier.action_direction_count, atelier.observe_direction_count,
                            atelier.observe_length_count, atelier.observe_length_step))
                    dat.write(repr(results))

            score = atelier.atelier.old_score
            non_null = np.count_nonzero(artist.table), np.count_nonzero(artist.table) / np.size(artist.table)
            print("Score: ", score)
            print("Non-Null:", non_null)

            if TRAIN:
                results.append([epsilon, score, no_epsilon_score, non_null[1]])

            if not TRAIN:
                atelier.atelier.show()
                time.sleep(1)

        if TRAIN:
            with open(os.path.join(directory, "results_{}.txt".format(filename.replace(".txt", ""))), "w") as dat:
                dat.write(
                    "# Training Values: Start: {}, Iterations: {}, Descent: {}, Minimum: {}\n".format(START, ITERATIONS,
                                                                                                      DESCENT, MINIMUM))
                dat.write(
                    "# Atelier Values: Action Direction: {}, Observation Direction: {}, Observation Lenght: {}, Observation Length Count: {}\n".format(
                        atelier.action_direction_count, atelier.observe_direction_count, atelier.observe_length_count,
                        atelier.observe_length_step))
                dat.write(repr(results))

            plot_results(results)

    except KeyboardInterrupt:
        pass

    if p:
        p.home()
        p.p.disconnect()

    if h:
        h.stop()
