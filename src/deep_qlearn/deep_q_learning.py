from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines import DQN
import time
import datetime
from deep_qlearn.circle_env import CircleEnv


class StableDeepQLearning:
    def __init__(self):
        pass

    def train(self, model_name: str = 'deep_circle.pkl', load=False, renders=False):
        """
        Trains reinforcement model to draw a circle.
        """
        env = CircleEnv(renders=renders, num_actions=64, always_print=False)
        env = DummyVecEnv([lambda :env])
        timestamp = time.time()
        if load:
            # Load an existing model
            model = DQN.load(model_name)
            model.set_env(env)
        else:
            # Create a new model
            model = DQN(MlpPolicy,env,
                        verbose=2,
                        buffer_size=1000,
                        exploration_fraction=0.3,
                        checkpoint_freq=1000,
                        checkpoint_path=".",
                        param_noise=True,
                        prioritized_replay=True)
        # Train the model
        model.learn(total_timesteps=150000)
        duration = time.time() - timestamp
        print("Time elapsed: {} minutes, {:.4f} seconds.".format(duration // 60, duration % 60))
        model.save(model_name)

    def test(self, model_name: str = 'deep_stable_bond_150k_64_always.pkl'):
        """
        Tests a trained model and displays the results.
        """
        env = CircleEnv(num_actions=64, always_print=True)
        env = DummyVecEnv([lambda: env])

        model = DQN.load(model_name)
        obs = env.reset()
        while True:
            action, _states = model.predict(obs)
            obs, reward, done, info = env.step(action)
            time.sleep(0.01)


if __name__ == '__main__':
    pass
