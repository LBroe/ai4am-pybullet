import math
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np

from vangogh import kandinsky


class CircleEnv(gym.Env):
    """
    Environment for an 2D circle.
    """

    def __init__(self,
                 max_steps: int = 100,
                 printer=None,
                 canvas_size: [int, int] = (200, 200),
                 num_actions: int = 32,
                 always_print: bool = True,
                 renders: bool = True):
        """
        Initializes the environment.
        :param max_steps: Max steps per episode
        :param printer: Kandinsky printer
        :param canvas_size: Size of the canvas
        :param num_actions: Number of actions
        :param always_print: Force agent to print always
        :param renders: Display the simulation
        """
        self.steps = 0
        self.max_steps = max_steps
        self.printer = printer
        self.radius_bounds = (40, 42)
        self.canvas_size = canvas_size
        self.num_actions = num_actions
        self.renders = renders

        self.atelier = kandinsky.KandinskyAtelier2(
            self.radius_bounds,
            canvas_size=canvas_size,
            printer=printer,
            deepq=True
        )

        # Action space
        self._action_count = self.num_actions if always_print else self.num_actions * 2
        self.action_space = spaces.Discrete(self._action_count)

        # Observation space
        bound_high = np.array(list((np.inf, np.inf)))
        self.observation_space = spaces.Box(-bound_high, bound_high, dtype=np.float32)

    def seed(self, seed=None):
        """
        Returns a random seed.
        """
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _action(self, i):
        """
        Calculates an action for a given action id.
        """
        print = (i < self.num_actions)
        i = i % self.num_actions
        x = math.cos(2 * np.pi / (self._action_count/2) * i)
        y = math.sin(2 * np.pi / (self._action_count/2) * i)
        return ((5 * x, 5 * y), print)

    def step(self, action_id: int):
        """
        Calculates the reward and observation for a given action id.
        """
        self.steps += 1
        _action = self._action(action_id)

        reward = self.atelier.step(*_action)
        observation = self.atelier.observe()

        abs_position = np.array(self.atelier.easel.brush)
        left_canvas = (np.any(abs_position < 0) or np.any(abs_position > self.canvas_size))
        done = self.steps >= self.max_steps or left_canvas

        # Observation, reward, done, info
        return observation, reward, done, {}

    def reset(self):
        """
        Resets the environment to start a new episode.
        """
        self.render()
        self.steps = 0
        self.atelier = kandinsky.KandinskyAtelier2(
            self.radius_bounds,
            canvas_size=self.canvas_size,
            printer=self.printer,
            deepq=True
        )
        observation = self.atelier.observe()
        return observation

    def render(self, mode='human'):
        """
        Displays the current state of the simulation.
        """
        if self.renders:
            self.atelier.show()

if __name__ == '__main__':
    pass
