from stick_balancing_2d import utils
import argparse
from stick_balancing_2d.deep_q_learning import DeepQLearning


def main():
    """ Configure parser"""
    parser = argparse.ArgumentParser(description="Use reinforcement learning to \
                                                  solve the pole cart problem.")
    parser.add_argument('-u', '--use', metavar='PATH',
                        type=str, help="Use an existing model.")
    parser.add_argument('-s', '--save', metavar='PATH', default="balance.pkl",
                        type=str, help="Path to save the trained model.")
    parser.add_argument('-n', '--no-graphic', action='store_false', default=True,
                        help="Display no graphical output.")

    args = parser.parse_args()
    dql = DeepQLearning()

    if args.use is not None:
        if utils.file_exists(args.use):
            dql.test(args.use)
        else:
            print("Model not found!")
    else:
        model_name = utils.get_empty_file_name(args.save, default="2d_balance.model")
        print(f"Model will be saved as {model_name}")
        dql.train(model_name, renders=args.no_graphic)


if __name__ == '__main__':
    main()
