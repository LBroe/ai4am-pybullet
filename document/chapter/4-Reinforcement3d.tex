\chapter{Reinforcement Learning für 3D-Drucker}
\label{ch:reinforcement3d}

Die in Kapitel \ref{ch:reinforcement} beschriebenen Reinforcement-Learning-Techniken sollen nun für 3D-Drucker Anwendung finden.
Dazu muss zuerst ein Modell aufgestellt werden, dass sich für die simple Simulation eines 3D-Druckers eignet (siehe Abschnitt \ref{druckermodell}).
Mit diesem kann dann ein konkretes Problem definiert werden, dass durch Reinforcement-Learning gelöst werden soll. In unserem Fall ist dies das Drucken eines Kreises (siehe Abschnitt \ref{section-problem-kreis}).
Dafür wird die Simulation um eine Reward-Funktion, die angibt, wie gut die Aufgabe erfüllt wird, und eine Observation zu einem Reinforcement-Learnning-Environment ergänzt.
Mit diesem Environment kann dann ein Reinforcement-Learning-Verfahren angewandt werden (siehe Abschnitt \ref{section-lernverfahren}), das einen Agent trainiert, der schrittweise lernt das gegebene Problem des Druckens von Kreisen zu lösen.
Das Environment kann dann an einen echten Drucker angebunden werden (siehe Abschnitt \ref{section-training-on-printer}), einerseits um die Eigenschaften eines echten Druckers in den Lernprozess zu integrieren, andererseits um letztendlich Resultate zu drucken.

\section{Druckermodell}\label{druckermodell}

Als Basis für ein Reinforcement-Learning-Verfahren wird eine Simulation
benötigt. Diese modelliert die Zustände, die die Umgebung einnehmen
kann, sowie deren Übergänge. Das heißt, für unsere gegebene
Problemstellung möchten wir den 3D-Drucker simulieren.

Dieses Modell soll als Basis dienen, auf der ein Reinforcement-Learning-Umgebung aufgesetzt werden kann.
Damit soll ein Agent lernen, mit dem Drucker umzugehen.
Im ersten Ansatz haben wir versucht, ein solches Model in 3D mit der PyBullet-Bibliothek umzusetzen (Abschnitt \ref{subsection-modell-pybullet}).
Danach sind wir dazu übergangen, ein simples zweidimensionales Modell zu nutzen (Abschnitt \ref{subsection-modell-2d}).

\subsection{PyBullet}
\label{subsection-modell-pybullet}

PyBullet ist eine Python-3-Anbindung an die Bullet Physics Library. Diese ist eine Physik-Engine, die Objekte im dreidimensionalen Raum simuliert, deren Bewegungen in der Welt oder relativ zueinander durch Constraints eingeschränkt werden können. Es können auch Motoren simuliert werden, durch die die Objekte direkten Einfluss auf die Umwelt ausüben.

PyBullet wird häufig als Environment für Reinforcement-Learning-Aufgaben eingesetzt, vor allem im Robotik-Bereich.

\begin{figure}[h]
	\centering
	\includegraphics[width=.5\linewidth]{img/wackelturm.png}
	\caption{Ein simulierter Zylinder, der wegen der zu schwachen Constraints zusammenbricht. Dieses Verhalten ist ein Artefakt der PyBullet-Simulation.}
	\label{fig:wackelturm}
\end{figure}

Der Hauptaspekt bei der physikalischen Simulation eines 3D-Druckers ist das Filament. Es muss am Druckbett und an anderem Filament haften und dort aushärten. Da PyBullet keine Flüssigkeitssimulation anbietet, muss das Filament durch kleine Kugeln simuliert werden, wobei die Viskositäts- und Adhäsionskräfte durch Constraints zwischen den Kugeln dargestellt werden.

Daraus ergibt sich das Problem mit PyBullet: Es gibt kein Constraint ohne Bewegungsspielraum. Bei mehreren reihenweise aneinander fixierten Kugeln zeigt sich schnell alleine durch die Gravitation eine starke Neigung, selbst wenn die Constraints darauf eingestellt sind, sämtliche Bewegungen zu unterbinden. Daraus folgt, dass wie in Abbildung \ref{fig:wackelturm} gezeigt ein Druck mit mehreren Schichten nicht realistisch simuliert werden kann.



\subsection{Zweidimensionales Modell}
\label{subsection-modell-2d}

Aufgrund unserer Erfahrungen mit dem PyBullet-Modell haben wir beschlossen, uns für das Projektseminar auf ein simpleres Modell zu beschränken.
Wir wollen nur das Drucken in einer waagerechten Ebenen modellieren.
Die Ebene wird implementiert durch ein zweidimensionales Array, in dem jede Zelle entweder leer oder mit Filament ausgefüllt ist.
Am Anfang sind alle Zellen im leeren Zustand.

Daneben gibt es einen Druckkopf, dargestellt durch Koordinaten für das Array.
Dieser Druckkopf kann sich über die Ebene bewegen.
Die Bewegungen werden durch relative Bewegungsbefehle gesteuert, was der Ansteuerung der Schrittmotoren in einem echten 3D-Drucker entspricht.
Bei der Bewegung kann er Filament ablegen, indem er die Zellen, über die er streift, in den ausgefüllten Zustand versetzt.

Ein solches Modell ist \glqq perfekt\grqq.
Es enthält keine Störquellen und alle Befehle haben ein deterministisches Ergebnis.
Das unterstützt zwar vermutlich den Lernprozess, verringert aber auch die Wahrscheinlichkeit,
dass der Agent Fehler, die während eines echten Drucks auftreten, geeignet ausgleichen kann.
Daran knüpft Abschnitt \ref{section-training-on-printer} an, in dem ein echter Drucker benutzt wird,
um reale Störquellen und ungenaues Verhalten in die Simulation zu integrieren.

\begin{figure}
	\centering
    \includegraphics[width=.5\linewidth]{img/hough_bad_before_trimmed.png}
	\caption{Simulierte Druckebene mit Beispieldruck}
	\label{fig:model-exapmle}
\end{figure}

\section{Modellproblem: Ein Kreis}
\label{section-problem-kreis}

%TODO inkonsistene Zeitformen

Als erstes Problem zur Demonstration von Reinforcement Learning für 3D-Drucker haben wir uns das Drucken eines zweidimensionalen Kreises auf dem Druckbett ausgesucht. Für das Problem wollten wir einen einfachen Q-Learning-Agent trainieren. Ein Kreis eignet sich gut, da Kreise rotationssymmetrisch sind und somit keine Unregelmäßigkeiten besitzen. Dies erleichtert sowohl das Überprüfen der Qualität als auch den Lernprozess, da der Agent nur lernt eine durchgehende Linie um einen Mittelpunkt herum zu zeichnen. Andere geometrische Formen besitzen bestimmte Kantenlängen und Winkel, welche der Agent zusätzlich lernen müsste.


\subsection{Environment basierend auf Hough-Transformation}
\label{subsection-env-hough}

Um festzustellen, wie sehr das Gezeichnete einem Kreis entspricht, bietet sich die übliche Technik zur Erkennung von Kreisen an: Die Hough-Transformation \cite{hough1962}.
Diesen Ansatz benutzen wir in unserer ersten Iteration des Reinforcement-Learning-Environments.
Dazu wird das Array der füllbaren Zellen, das als Schwarz-Weiß-Bild aufgefasst werden kann, nach jedem Schritt in den Hough-Raum für Kreise mit vorgegebenem Radius übertragen.

%Radius. Woher kommt der, wie fest
Der Radius wird einmal festgelegt und bleibt dann zwischen mehreren Simulationsläufen konstant.
Der Agent lernt also nur Kreise dieser Größe zu zeichnen.
Für Kreise anderer Größe muss ein neuer Agent trainiert werden.

Die Belohnung für den Agent soll auf der Güte des gezeichneten Kreises basieren.
Diese lässt sich durch das Maximum im Hough-Raum bestimmen, das dem Punkt entspricht, der auf dem simulierten Druckbett am ehesten dem Mittelpunkt eines Kreises mit dem gegebenen Radius darstellt.
Nach jedem Schritt wird der Agent für die Erhöhung des Maximalwerts belohnt.
Hierbei kann die Position des Höhepunkts zwar springen, der Wert steigt aber monoton, da das Filament nur hinzugefügt aber nicht gelöscht werden kann.
Gleichzeitig wird der Agent für jede Bewegung pauschal bestraft, wodurch es für den Agent am effizientesten ist, konsequent an einem Kreis zu arbeiten und diesen verbessern.

Auch die Beobachtung, die dem Agent übergeben wird, basiert auf der Hough-Transformation.
Der Agent beobachtet die Position des Hough-Maximums relativ zur Position des Druckkopfes
und muss alleine basierend auf dieser Information entscheiden, welche Aktion als nächstes durchgeführt wird.
Wie oben erwähnt kann dieser Höhepunkt von Schritt zu Schritt mehr oder weniger beliebig springen, was der Agent nicht nachzuvollziehen kann, da er kein Gedächtnis hat und immer nur den aktuellen Höhepunkt beobachtet.
Für das Drucken eines Kreises ist eine konsequente Strategie notwendig,
was aber nur schwer funktionieren kann, wenn die Beobachtung des Agents unstetig ist.

Ein zusätzliches Problem bei der Implementierung des Hough-Environ\-ments
ist die Tatsache, dass eine solche Hough-Transformation aufwändig ist.
Der Agent soll eigentlich sehr viele Aktionen durchführen und sehr viel beobachten, um über viele Simulationsläufe hinweg viel zu lernen.
Wenn bei jedem Schritt eine solche aufwändige Transformation durchgeführt wird,
verlangsamt das den Prozess.
Das Problem konnte teilweise behoben werden, indem alte Hough-Transformationen wiederverwendet werden.
Dabei wird die letzte Transformation um die Änderungen ergänzt, die sich bei den Unterschieden im Urbildraum seit der letzten Transformation ergeben.
Dennoch bleibt die Transformation ein Hindernis für schnelle Lernprozesse.

\begin{figure}
	\centering
	\subfigure{
		\centering
		\includegraphics[width=.35\linewidth]{img/hough_bad_before_trimmed.png}
	}
	\subfigure{
		\centering
		\includegraphics[width=.35\linewidth]{img/hough_bad_unblurred_trimmed.png}
	}
	\subfigure{
		\centering
		\includegraphics[width=.35\linewidth]{img/hough_before_trimmed.png}
	}
	\subfigure{
		\centering
		\includegraphics[width=.35\linewidth]{img/hough_unblurred_trimmed.png}
	}
	\caption{Drucksimulation (links) und deren Hough-Transformation (rechts) eines schlechten (oben) und eines guten (unten) Agents}
	\label{fig:hough}
\end{figure}

\subsection{Environment basierend auf festem Mittelpunkt}

Nachdem die Hough-Methode keine guten Ergebnisse geliefert hat,
haben wir das Verfahren zur Bestimmung des Rewards in einer neuen Iteration geändert.
In diesem Verfahren wird bestimmt, ob ein Kreis um einen vorgegebenen festen Punkt $M$ vorhanden ist und wie stark er ausgeprägt ist.
$M$ wird vor dem Start der eigentlichen Simulation festgelegt.

Hierbei wird die Problemstellung anders interpretiert.
Es soll immer noch ein Kreis mit festem vorgegebenen Radius gezeichnet werden.
Allerdings muss der Agent nicht selbst wählen, wo der Kreis entstehen soll.
Stattdessen kommt dazu eine Vorgabe von außen.
Auf diese Weise wird das Problem aus der Sicht des Agents einfacher,
obwohl es weniger potentielle Lösungen gibt.

%Beobachtung
Die Beobachtung des Agents ist die relative Position von $M$ zu sich selbst.
Dabei kann dieser Punkt, anders als der Hough-Höhepunkt, zwischen den Schritten nicht beliebig springen.
Die einzige Veränderung der Beobachtung entsteht im direkten Zusammenhang mit den Aktionen des Agents.
So dient $M$ dem Agent als feste Referenz, an der er sich orientieren kann,
um konsequent zu lernen, einen Kreis um einen solchen vorgegebenen Punkt zu zeichnen.

%Reward
Für den Reward muss die Güte des Kreises um $M$ bestimmt werden.
Prinzipiell bietet sich dafür immer noch die Hough-Transformation an (vgl. \ref{subsection-env-hough}).
Wieder basiert die Belohnung auf der Erhöhung eines Wertes im Hough-Raum.
Aber der Wert ist nicht das Maximum im jeweiligen Raum sondern Werte an einer festen Stelle.
Da dieser Punkt nicht von der Hough-Transformation abhängt, lässt sich die gesamte Auswertung auf die Berechnung dieses einen Wertes reduzieren.
Der Wert lässt sich dadurch approximieren,
dass alle ausgefüllten Array-Felder in einem Radius um $M$ gezählt werden.
Wie in der vorigen Iteration wird der Agent für die Erhöhung dieses Wertes belohnt.
Dieser Wert kann nicht sinken, denn Array-Zellen können nicht vom ausgefüllten in den unausgefüllten Zustand wechseln.

%Zellen außerhalb unbeachtet, nur opportunität

%Steuerung
Diese Methode hat den zusätzlichen Vorteil, dass der trainierte Agent durch das Setzen des Mittelpunktes noch gesteuert werden kann.\\[-0.7cm]

\subsubsection{Lenkung auf die Kreisbahn}

%Bestrafung für Nicht-auf-dem-Kreis-sein
Der Reward für Bewegungen weit außerhalb des Kreises, sowie mittendrin, ist ``flach'':
Bei jeder beliebigen durch den Agent gewählten Richtung wird nicht auf der Kreisbahn gezeichnet,
die Bewertung des Kreises erhöht sich nicht und deshalb ist der Reward immer Null.
Das erschwert es, zu lernen, was die beste Vorgehensweise ist.
Um diese Situation zu verhindern, gibt es einen sekundären Reward, der viel kleiner gewichtet ist.
Dabei wird der Agent belohnt, je näher der Druckkopf der Kreisbahn ist,
wodurch er lernt, den Druckkopf dorthin zu bewegen,
um den höheren primären Reward für das Ausfüllen der Kreisbahn zu erhalten.

%theoretisch nicht nötig
Theoretisch ist dieser sekundäre Reward nicht notwendig,
denn Q-Learning und verwandte Algorithmen sind durchaus imstande,
Entscheidungen zu treffen,
um einen weiter in der Zukunft liegenden Reward zu erhalten.
Aber dieser Zusammenhang muss im Laufe des Lernprozesses aufwändig hergestellt werden.
Also beschleunigen wir den Prozess durch unseren sekundären Reward und sparen Rechen-Ressourcen,
ohne dabei das Ergebnis substantiell zu verändern.\\[-0.7cm]

\subsubsection{Randomisierter Start}

Der Agent soll lernen mit allen Situationen umgehen zu können. Dazu wird am Anfang jeder Episode der Kreismittelpunkt $M$ zufällig gewählt. Der Druckkopf hingegen startet immer in der Mitte der Ebene. Da die Beobachtung des Agents abhängig ist von $M$, ist für ihn jeder Start ein anderer.
Durch einen zufälligen Start ist der Agent in der Lage schneller zu lernen, da er öfter einer neuen Situation ausgesetzt ist.

\subsubsection{Relative Orientierung}

%Da das gewünschte Ergebnis ein Kreis ist, kann sowohl die Observation, als auch die Action des Agents rotationsunabhängig umgesetzt werden. Dabei werden wie in Abbildung \ref{fig:rotationsinvariant} gezeigt die Winkel der Observation und der Action relativ zur letzten Laufrichtung des Agents angegeben.

\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{img/rotationalinvariant.pdf}
	\caption{Ein Druckkopf $A$ mit vorgegebenem Kreismittelpunkt $M$, vorheriger Laufrichtung $p$, Observation $(l, \theta)$, Action $\alpha$ und resultierender Laufrichtung $n$. Aufgrund der Rotationsinvarianz ist die absolute Position des Druckkopfs irrelevant.}
	\label{fig:rotationsinvariant}
\end{figure}

Die Problemstellung des Zeichnens eines Kreises um den Punkt $M$ ist rotationsinvariant,
weil Kreise rotationssymmetrisch sind.
Deshalb kann die Orientierung der Beobachtung und der Aktion des Agents relativ zur letzten Laufrichtung interpretiert werden,
ohne dass das Einfluss auf die Korrektheit der Aktion hat (siehe Abbildung \ref{fig:rotationsinvariant}).
Das heißt, dass die Information der Orientierung bezüglich eines aus für das Problem irrelevanten Bezugsrahmens ersetzt wird durch die Orientierung nach der letzte Laufrichtung.
Der Agent wird von überflüssigen Informationen befreit, aber die Modellierung von Beobachtung und Aktion als Vektor ändert sich nicht.

Gleichzeitig gibt die relative Orientierung dem Agent die Möglichkeit
zu lernen, die Richtung beim Zeichnen des Kreises nicht zu ändern.
Bei einem Winkel von $0^\circ$ bewegt sich der Drucker in dieselbe Richtung weiter
und bei einem Winkel von $180^\circ$ bewegt er sich zurück.
Ohne relative Orientierung ist diese Unterscheidung so nicht möglich
und die beiden Strategien, den Kreis im Uhrzeigersinn und gegen den Uhrzeigersinn zu zeichnen, konkurrieren miteinander.
Mit relativer Orientierung kann der Agent beide Strategien gleichzeitig beherrschen,
eine zufällig wählen und konsequent durchführen.

\subsection{Diskretisierung}
\label{section-diskretisierung}

Für die Anwendung des Q-Learning-Algorithmus müssen Beobachtung und Aktion des Agents diskretisiert werden,
denn dadurch wird die Benutzung von Tabellen in diesem Verfahren ermöglicht (siehe Abschnitt \ref{subsection-q-learning}).
Die Definition des Environments basiert bisher aber auf stetigen Vektoren.
Diese müssen also durch Abzählen von Möglichkeiten durchnummeriert und somit diskretisiert werden.

Die Beobachtung der in den vorigen Abschnitten beschriebenen Environments ist die relative Position eines Punktes zum Druckkopf.
Dieser Vektor wird zuerst in Polar-Koordinaten zerlegt.
Dann wird der Winkel einem von $c_{dir}$ Teilen des Einheitskreises und die Länge einem von $c_{count}$ Segmenten mit einer Länge von je $c_{len}$ zugeordnet, wobei Längen größer oder gleich $c_{count} \cdot c_{len}$ auch auf $c_{count} - 1$ abgebildet werden. Insgesamt ergeben sich die Abbildungen $disc_{dir}: [0, 2\pi) \to \{0, \dots, c_{dir} - 1\}$ und $disc_{len}: \mathbb{R}_{\ge 0} \to \{0, \dots, c_{count} - 1\}$. Es gilt:
\begin{align*}
    disc_{dir}(\theta) &= \left\lfloor \frac{\theta}{2\pi} \cdot c_{dir} \right\rfloor \\
disc_{len}(l) &= \min\left\{\left\lfloor \frac{l}{c_{len}} \right\rfloor, c_{count} - 1 \right\}
\end{align*}
Insgesamt wird die Beobachtung des Agents im Environment durch die Funktion $disc: [0, 2\pi) \times \mathbb{R}_{\ge 0} \to \{0, \dots, c_{dir} \cdot c_{count} - 1\}$  mit 
$$
disc(\theta, l) = disc_{len}(l) + disc_{dir}(\theta) \cdot c_{count}
$$
in eine natürliche Zahl übertragen, die eine Zeile einer Q-Learning-Tabelle adressiert.

Analog muss die Aktion des Agents diskretisiert werden.
Dabei wird die Länge des Bewegungsvektors festgelegt.
Der Winkel wird aus einem von $a_{dir}$ gleichmäßig im Einheitskreis verteilten Winkeln gewählt.
Der Agent wählt also eine Zahl basierend auf seiner Tabelle, die dann in einen Winkel und somit in den Aktionsvektor übertragen wird.

%abwägung parameter
Die Parameter $c_{dir}$, $c_{len}$, $c_{count}$ und $a_{dir}$ können prinzipiell beliebig gewählt werden.
Sie bestimmen wie genau der Agent arbeiten kann.
Werden sie hoch eingestellt, verbessert dies die Ergebnisse, aber die Tabelle des Q-Learning-Verfahrens wird größer und der Lernprozess dauert entsprechend länger.
Werden die Parameter klein gewählt, beschleunigt dies das Lernen, aber die maximal mögliche Qualität der gedruckten Kreise sinkt.

\section{Lernverfahren}
\label{section-lernverfahren}

\subsection{Q-Learning}
\label{subsection-q-learning}
Mit der erstellten Environment können wir ein Reinforcement-Learning-Verfahren anwenden. Wir haben uns zuerst für Q-Learning (siehe \ref{sec:q-learning}) entschieden.
Aufgrund der Diskretisierung von Beobachtung und Aktion (siehe \ref{section-diskretisierung}) können wir die Action-Value-Function in einer Tabelle speichern. Hierbei steht für jede mögliche Beobachtung im Environment eine Zeile und für jede mögliche Aktion des Agents eine Spalte.
Die Wahl der ausgeführten Aktion erfolgt mittels $\varepsilon$-greedy-Verfahren (vgl. Formel \ref{eq:epsilon-greedy}). Das $\varepsilon$ ist zu Beginn des Lernens hoch, um einen möglichst großen Teil der Zustände zu erkunden. Über die Zeit wird $\varepsilon$ kleiner geregelt, sodass die Aktionen gewählt werden, welche die besten Resultate erzielt haben.
Mit der Formel \ref{eq:q_learning} lassen sich anhand der gewählten Aktion, der neuen Beobachtung und des erhaltenen Rewards Einträge in der Tabelle füllen.
Nachdem der Agent trainiert wurde, war er in der Lage, kreisförmig um den Punkt $M$ zu zeichnen. Die Kreise waren dabei allerdings nicht perfekt, da der Agent nicht in jeder Situation die beste Strategie kannte.
Durch das Trainieren weiterer Agents mit geänderter Lernrate $\alpha$ und Discount-Faktor $\gamma$ sowie geändertem Verhalten von $\varepsilon$ über die Zeit konnten wir Agents trainieren welche z.T. bessere Strategien gelernt haben.  

\subsection{Deep-Q-Learning}
\label{subsection-deep-q-learning}
Um die Mächtigkeit approximativer Verfahren auch im Kontext des 3D-Drucks (bzw. dessen Simulation) zu testen, haben wir das im vorigen Kapitel besprochene DeepQ-Learning (vgl. Kapitel \ref{deepq-learning}) zum Trainieren eines Agents in der Kreis-Environment verwendet. Die von uns verwendete Implementierung von Stable Baselines benötigt allerdings für das Training eine Gym-Environment, sodass eine direkte Anbindung wie für das Q-Learning nicht möglich ist. Wir haben daher einen Wrapper geschrieben, der die von der Simulation bereitgestellte Funktionalität kapselt und die entsprechenden Methoden und Signaturen einer Gym-Environment für den Algorithmus bereitstellt. Wir haben dabei Parameter der Simulation dem Wrapper als Parameter gegeben, sodass beispielsweise die Anzahl der verwendeten Actions leicht anpassbar ist. Weiterhin ist es auch möglich, die grafische Simulation explizit zu deaktivieren, was den Trainingsprozess erheblich beschleunigt.
%TODO

\section{Anbindung eines echten 3D-Druckers}
\label{anbindung-printer}

Die Simulation kann an einen echten 3D-Drucker angebunden werden.
Dabei wird die Bewegung des simulierten Durckkopfes an sein echtes Gegenstück gesendet.
Dadurch ist es möglich, die vom Agent gelernten Bewegungen zu nuzten, um echte Kreise zu drucken.

Ein ausschließlich in einer Simulation trainierter Agent ist wegen Eigenschaften der Hardware, wie beispielsweise verrutschenden Antriebsgurten oder falsch reagierenden Motoren, eventuell nicht gut geeignet, einen tatsächlichen 3D-Drucker zu steuern.
Daher möchten wir das Training direkt auf dem Drucker durchführen.
Hierfür wird ein vom Agent gegebener Befehl erst an den Drucker übertragen, die Position des Druckers wird von der Kamera gemessen und die gemessene Bewegung wird dann in die Simulation überführt, wo der Reward und die Observation berechnet werden.
Auf diese Weise lernt der Agent mit dem realen inexakten Verhalten des Druckers und den Ungenauigkeiten des Trackings sowie der Kamera umzugehen.

\subsection{Training auf dem Drucker}
\label{section-training-on-printer}

Bei dem Trainieren des Agents auf dem 3D-Drucker gibt es einige Dinge zu beachten. Bevor man das Skript startet sollten die Trainings-Hyper\-parameter gewählt werden und die Variablen \texttt{Train} und \texttt{Print} auf True gesetzt werden. \texttt{Print} gibt dabei lediglich an, dass das Training auf dem angeschlossenen Drucker stattfindet. Wenn man möchte, dass der Drucker tatsächlich Filament druckt, muss man beim Konstruktor des Printers den Parameter \texttt{heat} auf true setzen. Allerdings sollte dies nur am Ende getan werden, wenn man das Ergebnis des Trainings überprüfen möchte. Auch ist es sinnvoll sich entweder nach jeder Iteration oder in festen Zeitabständen auf der Konsole ausgeben zu lassen, ob das Tracking immer noch erfolgreich ist. Ansonsten kann es passieren, dass aufgrund eines Fehlers das Tracking gescheitert ist und der Agent kein korrektes Feedback über die ausgeführte Bewegung des Druckers bekommt. Das würde das Training des Agents zerstören.

Leider haben wir erst gegen Ende einen Versuch starten können, den Agent ausschließlich auf dem Drucker zu trainieren. Wir haben nach 1500 Iterationen leider keine brauchbaren Ergebnisse erhalten. Der Agent war nicht ansatzweise in der Lage einen Kreis oder einen Teil eines Kreises zu drucken. Die Ergebnisse sind in Abschnitt \ref{s:ergebnisse-training-drucker} zu sehen.\\[0.3cm]

\subsection{Steuerung des Druckkopfes}

3D-Drucker werden durch die Programmiersprache G-Code gesteuert.
Die G-Code-Instruktionen können über USB an den Drucker gegeben werden, sodass er vom Computer direkt angesprochen werden kann.
Da unsere Simulation genau wie der Drucker durch relative Bewegungsbefehle kontrolliert wird, kann die Steuerung der Simulation eins-zu-eins auf den Drucker gespiegelt werden.

\subsubsection{Synchronisation von Drucker und Sensoren}
\label{subsubsection-synchronize-printer}

Für akkurates Feedback muss die Bewegung des Druckers beendet sein, bevor die Sensoren die Position bestimmen. Dafür kann nach jeder Bewegung ein Befehl eingefügt werden, der auf die Vollendung der vorherigen Befehle wartet und anschließend eine Rückmeldung an den Computer gibt, der dann die Sensoren ausliest und die nächste Bewegung berechnet. Da der Befehl eine leichte Verzögerung hat, hält der Drucker kurz an, bevor er eine neue Bewegung starten kann, was eine starke Unregelmäßigkeit des Drucks verursacht.

Die bisher beste Lösung war es, die Dauer einer Bewegung, die immer die gleiche Länge hat, zu schätzen und vor jedem Befehl unreflektiert eine feste Zeitspanne zu warten.

Ein Vergleich der beiden Methoden ist in Abbildung \ref{fig:circles} zu sehen.

\begin{figure}
	\centering
	\subfigure[Warten auf Rückmeldung des Druckers]{
		\qquad
		\includegraphics[width=.35\linewidth]{img/circle_dots.jpg}
		\qquad
	}
	\subfigure[Schätzen der Bewegungsdauer]{
		\qquad
		\includegraphics[width=.35\linewidth]{img/circle_continuous.jpg}
		\qquad
	}
	\caption{Vergleich der beiden Methoden zur Druckersynchronisation}
	\label{fig:circles}
\end{figure}
