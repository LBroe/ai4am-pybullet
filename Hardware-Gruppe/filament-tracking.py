import cv2
from skimage.measure import compare_ssim
from pyimagesearch import imutils


# script to monitor whether the printed object has peeled of
# and moved causing a print failure
# therefore it uses the attached front camera moving with the printbed


ImageSizeX = int(640)
ImageSizeY = int(480)

# color boundaries for the hsv color space
# for red 4 boundaries are needed because red is split in two halves in hsv color space
colorLower1 = (0, 86, 50)  # red
colorUpper1 = (15, 255, 255)  # red
colorLower2 = (165, 86, 50)  # red
colorUpper2 = (180, 255, 255)  # red
# colorLower1 = (29, 86, 20) #green
# colorUpper1 = (64, 255, 255) #green

vs = cv2.VideoCapture(0)
oldFrame = None
counter = 0

while True:
    ret, frame = vs.read()

    if not ret:
        break

    frame = imutils.resize(frame, width=ImageSizeX)
    blurred = cv2.GaussianBlur(frame, (21, 21), 0, 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    # threshold the image according to the color boundaries
    frame1 = cv2.inRange(hsv, colorLower1, colorUpper1)
    frame2 = cv2.inRange(hsv, colorLower2, colorUpper2)
    # combine the two frames
    frame = frame1 | frame2
    frame = cv2.erode(frame, None, iterations=8)
    frame = cv2.dilate(frame, None, iterations=8)

    # compare the actual image with the image 30 frames before and compute
    # the structural similarity index of these two images
    if oldFrame is not None and counter % 30 == 0:
        (score, diff) = compare_ssim(oldFrame, frame, full=True)
        # difference image
        diff = (diff * 255).astype("uint8")
        print("SSIM: {}".format(score))
        # if the SSIM is under 0.97 it is possible that the print object has moved
        if score < 0.97:
            print("Possible Failure!")
        # threshold difference image
        diff = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
        cv2.imshow("Diff", diff)

    cv2.imshow("Frame", frame)
    if counter % 30 == 0:
        oldFrame = frame
        counter = 0
    key = cv2.waitKey(1) & 0xFF
    counter += 1

    if key == ord("q"):
        break

vs.release()
cv2.destroyAllWindows()
