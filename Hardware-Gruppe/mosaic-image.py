import numpy as np
import argparse
import re
from imutils import paths
import cv2

# script to create a mosaik image out of 20 pictures
# pictures have to be in correct order starting in the top right corner
# the first 4 pictures are the first row and so on


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images", type=str, required=True,
                help="path to input directory of images to stitch")
args = vars(ap.parse_args())

# function to sort the images by name correctly
def sorted_nicely(l):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


counter = 0

print("[INFO] loading images...")
imagePaths = sorted_nicely(list(paths.list_images(args["images"])))
images = []

for imagePath in imagePaths:
    image = cv2.imread(imagePath)
    images.append(image)

if not len(images) > 0:
    print("No Images found!")
elif len(images) != 20:
    print("Number of images is not 20!")
else:
    imageSizeY, imageSizeX, channels = images[0].shape
    X1 = 0
    X2 = imageSizeX
    Y1 = 0
    Y2 = imageSizeY

    newImage = np.zeros(((5 * imageSizeY), (4 * imageSizeX), 3), dtype=np.uint8)

    for image in images:
        newImage[Y1:Y2, X1:X2] = image
        X1 += imageSizeX
        X2 += imageSizeX
        counter += 1
        # next row of pictures
        if counter % 4 == 0:
            counter = 0
            X1 = 0
            X2 = imageSizeX
            Y1 += imageSizeY
            Y2 += imageSizeY

    cv2.imshow("newImage", newImage)
    cv2.imwrite("mosaic.png", newImage)
    cv2.waitKey(0)
