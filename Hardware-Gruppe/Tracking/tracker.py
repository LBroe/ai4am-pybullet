from collections import deque
import numpy as np
import cv2
import pyrealsense2 as rs
import encoder
import time

# class with tracking function
class Tracker:

    def __init__(self, algorithm):
        self.algorithm = algorithm

    def track(self):
        # opencv tracking algorithms which can be chosen
        OPENCV_OBJECT_TRACKERS = {
            "csrt": cv2.TrackerCSRT_create,
            "kcf": cv2.TrackerKCF_create,
            "boosting": cv2.TrackerBoosting_create,
            "mil": cv2.TrackerMIL_create,
            "tld": cv2.TrackerTLD_create,
            "medianflow": cv2.TrackerMedianFlow_create,
            "mosse": cv2.TrackerMOSSE_create
        }

        # initialize multi tracker
        trackers = cv2.MultiTracker_create()

        # Size of the drawing window
        ImageSizeX = int(960)
        ImageSizeY = int(540)

        # queues for the tracked center points of the bounding boxes with length 64
        pts1 = deque(maxlen=64)
        pts2 = deque(maxlen=64)
        enc = encoder.Encoder()
        enc.start()
        counter = 0  # counter used to number the output drawing pictures

        # configuration of the realsense-camera
        pipeline = rs.pipeline()
        config = rs.config()
        config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8,
                             60)  # args: stream type, width, height, format, fps
        pipeline.start(config)

        # variables for the drawing
        blackImage = np.zeros((ImageSizeY, ImageSizeX, 3), dtype=np.uint8)
        cv2.namedWindow("Drawing", cv2.WINDOW_AUTOSIZE)
        oldPoint = (int(ImageSizeX / 2), int(ImageSizeY / 2))
        newPoint = (int(ImageSizeX / 2), int(ImageSizeY / 2))

        # streaming loop
        while True:
            frames = pipeline.wait_for_frames()
            image = frames.get_color_frame()
            frame = np.asanyarray(image.get_data())
            # flip the frame so it matches the front view of the printer
            frame = cv2.flip(frame, 1)

            (success, boxes) = trackers.update(frame)

            for box in boxes:
                if success:
                    (x, y, w, h) = [int(v) for v in box]
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            # bounding box of the print head
            if len(boxes) > 0:
                b0 = boxes[0]
                (x, y, w, h) = [int(v) for v in b0]
                center0 = (int(x + w / 2), int(y + h / 2))
                pts1.appendleft(center0)  # append the list of tracked points of the print head


            # bounding box of the marker on the printbed
            if len(boxes) > 1:
                b1 = boxes[1]
                (x, y, w, h) = [int(v) for v in b1]
                center1 = (int(x + w / 2), int(y + h / 2))
                pts2.appendleft(center1)  # append the list of tracked points of the printbed

            # calculate the distance between consecutive points for the drawing
            if len(pts1) > 2 and len(pts2) > 2:
                if not (pts1[0] is None or pts1[1] is None):
                    diffX = int(pts1[0][0] - pts1[1][
                        0])  # pts1[number of point in the list, dimension of the point eg. 0 for X and 1 for Y]
                    if newPoint[0] + diffX > ImageSizeX:
                        newPoint = (ImageSizeX, newPoint[1])  # set windowsize as limit
                    elif newPoint[0] + diffX < 0:
                        newPoint = (0, newPoint[1])
                    else:
                        newPoint = (newPoint[0] + diffX, newPoint[1])

                if not (pts2[0] is None or pts2[1] is None):
                    # 1.33 is the factor to adjust aspect ratio of the image
                    diffY = int(round(1.33 * (pts2[0][1] - pts2[1][1])))
                    if newPoint[1] + diffY > ImageSizeY:
                        newPoint = (newPoint[0], ImageSizeY)
                    elif newPoint[1] + diffY < 0:
                        newPoint = (newPoint[0], 0)
                    else:
                        newPoint = (newPoint[0], newPoint[1] + diffY)

            # draw the line
            cv2.line(blackImage, oldPoint, newPoint, (0, 0, 255), 2)
            oldPoint = newPoint

            # display the image and the drawing
            cv2.imshow("Frame", frame)
            cv2.imshow("Drawing", blackImage)
            key = cv2.waitKey(1) & 0xFF

            # if the encoder position increases more than 9 steps there was a layer change so we
            # save the drawing of this layer and then reset the encoder
            if enc.reader.position > 9:
                cv2.imwrite("Images/test/" + str(counter) + ".jpg", blackImage)
                counter += 1
                enc.reset()
                time.sleep(0.3)  # to ensure the encoder is reset before next iteration of the while loop
                blackImage = np.zeros((ImageSizeY, ImageSizeX, 3), dtype=np.uint8)
                oldPoint = (int(ImageSizeX / 2), int(ImageSizeY / 2))
                newPoint = (int(ImageSizeX / 2), int(ImageSizeY / 2))

            # option to manually take a picture with the p-key
            if key == ord("p"):
                cv2.imwrite("Images/" + "picture-" + str(counter) + ".jpg", blackImage)

            # quit with q-key
            if key == ord("q"):
                break

            # clear the drawing with the c-key
            if key == ord("c"):
                blackImage = np.zeros((ImageSizeY, ImageSizeX, 3), dtype=np.uint8)
                oldPoint = (int(ImageSizeX / 2), int(ImageSizeY / 2))
                newPoint = (int(ImageSizeX / 2), int(ImageSizeY / 2))
                enc.reset()

            # select the bounding boxes for the objects to be tracked
            if key == ord("s"):
                bboxes = []
                while True:
                    # select the bounding box of the object we want to track (make
                    # sure you press ENTER or SPACE after selecting the ROI)
                    box = cv2.selectROI("Frame", frame, fromCenter=False, showCrosshair=True)
                    bboxes.append(box)
                    key = cv2.waitKey(0) & 0xFF
                    if key == ord("q"):
                        break
                for bbox in bboxes:
                    # create a new object tracker for the bounding box and add it
                    # to our multi-object tracker
                    tracker = OPENCV_OBJECT_TRACKERS[self.algorithm]()
                    trackers.add(tracker, frame, bbox)
                    print(bbox)

        pipeline.stop()
        enc.stop()
        cv2.destroyAllWindows()
