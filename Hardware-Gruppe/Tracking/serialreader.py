import re
import serial

# Serial Reader Class to read out the serial port to get the position from
# the Linear Glass Scale


class SerialReader:

    def __init__(self):
        self.position = 0
        self.arduino = serial.Serial("/dev/ttyUSB0", timeout=1, baudrate=9600) # initiate the connection to the arduino
        self.running = True

    def read(self):
        while self.running:
            s = self.arduino.readline().decode('utf-8')
            # use reg expression to filter out just the numbers
            result = [int(d) for d in re.findall(r'-?\d+', s)]
            if len(result) > 0:
                self.position = result[0]
            print(self.position)
