import argparse
import tracker

# script which tracks the movement of the printer and saves an image
# of each layer automatically
# needs the arduino and the intel real sense camera to be connected

# argument parser which takes the tracking algorithm as an argument,
# if the argument is not given the default algorithm KCF is used
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--tracker", type=str, default="kcf", help="OpenCV object tracker type")
args = vars(ap.parse_args())

tracker = tracker.Tracker(args["tracker"])

tracker.track()
