import threading
import serialreader


# Encoder Class for the Linear Glass Scale/Encoder,
# which has a serial port reader as an attribute

class Encoder(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.reader = serialreader.SerialReader()

    def run(self):
        self.reader.read()

    def reset(self):
        self.reader.position = 0
        self.reader.arduino.write(b"reset")

    def stop(self):
        self.reader.running = False
